# Application : api-beta

api-beta est un projet de développement d'une api avec nodejs et expressjs.

## Installation

Utilisez [npm](https://pip.pypa.io/en/stable/) pour installer api-beta.

```bash
npm install
```

## Lancement

```bash
npm start
```

L'application se lance à l'adresse suivante : [localhost](http://localhost:3000).

## Auteurs

Ben & Mat.

## Licence
Fun For Free ...