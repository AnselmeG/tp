var chai = require('chai');
var chaiHttp = require('chai-http');

var should = chai.should();
var expect = chai.expect;

var Joi = require('joi');

chai.use(chaiHttp);

// mock
const auth_data = {
    user_firstname : 'world',
    user_lastname: 'hello',
    user_mail: 'hello@world.fr',
    user_birthdate: 'yyyy-mm-dd',
    user_password: 'helloworld',
    job_name: 'BACKEND',
    job_status: 'EN POSTE'
};

// tests
describe('Authentification', () => {

    it('Les différentes informations pour l inscription sont renseignés', (done) => {
        expect(auth_data).to.include.keys([
            'user_firstname', 
            'user_lastname',
            'user_mail',
            'user_password',
            'user_birthdate',
            'job_name',
            'job_status'
        ]
        );
        done();
    });

    it('Les valeurs des informations requises pour l inscription sont de type string', (done) => {
        expect(auth_data.user_firstname).to.be.a('string');
        expect(auth_data.user_lastname).to.be.a('string');
        expect(auth_data.user_mail).to.be.a('string');
        expect(auth_data.user_birthdate).to.be.a('string');
        expect(auth_data.user_password).to.be.a('string');
        expect(auth_data.job_name).to.be.a('string');
        expect(auth_data.job_status).to.be.a('string');
        done();
    });
});
