var chai = require('chai');
var chaiHttp = require('chai-http');

var should = chai.should();
var expect = chai.expect;

var Joi = require('joi');

chai.use(chaiHttp);

// mock
const login_data = {
    user_login : 'mtd42@gmail.com',
    user_password: 'hello',
};

// tests
describe('Inscription', () => {
    it('Les clés user_login et user_password sont renseigés', (done) => {
        expect(login_data).to.include.keys(['user_login', 'user_password']);
        done();
    });
    it('Les valeurs de user_login et user_password sont de type string', (done) => {
        expect(login_data.user_login).to.be.a('string');
        expect(login_data.user_password).to.be.a('string');
        done();
    });
});
