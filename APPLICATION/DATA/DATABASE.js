import mysql from 'mysql';
import util from 'util';

const db_params = {
    host: '82.64.98.7',
    user: 'pysql',
    password: 'pysql',
    database : 'PROD',
    port : '3306'
};

export const database = mysql.createPool(db_params);

database.query = util.promisify(database.query);