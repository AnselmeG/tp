import {
    QUERY_profil_by_id,
    QUERY_profil_update,
} from '../MODELS/QUERIES/profil/QUERIES_profil.js';


const MODEL_profil = async (user_id) => {
    try {
        await QUERY_profil_by_id(user_id);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state: 'internal_server',
                _internal_server : resource.sqlMessage
            };
        } else {
            throw {
                _state: 'found',
                _resource: resource
            };
        }
    } 
};

const MODEL_profil_update = async (profil_data) => {
    try {
        await QUERY_profil_update(profil_data);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state : 'internal_server',
                _internal_server : resource.sqlMessage,
            };
        } else if (resource._state === 'created') {
            throw {
                _state : 'created',
                _created : 'User created',
            };
        } else {
            throw {
                _state : 'conflict',
                _conflict : 'User already exit'
            };
        }
    }
};


export {
    MODEL_profil,
    MODEL_profil_update,
};