import {
    database
} from '../../../DATABASE.js';


import _ from 'underscore';


import getAge from 'get-age';
 
class Profil {
    constructor(USER_FIRSTNAME, USER_LASTNAME, USER_BIRTHDATE, JOB_NAME, JOB_STATUS) {
        this.USER_FIRSTNAME = USER_FIRSTNAME;
        this.USER_LASTNAME = USER_LASTNAME;
        this.USER_BIRTHDATE = getAge(USER_BIRTHDATE).toString();
        this.JOB_NAME = JOB_NAME;
        this.JOB_STATUS = JOB_STATUS;
    }
}


const QUERY_profil_by_id = async (user_id) => {
    const query = `SELECT 
    USER_FIRSTNAME, USER_LASTNAME, USER_BIRTHDATE, JOB.JOB_NAME, JOB.JOB_STATUS
    FROM USER
    JOIN PROFIL ON USER.PROFIL_ID = PROFIL.PROFIL_ID
    JOIN JOB ON PROFIL.PROFIL_ID = JOB.JOB_ID
    WHERE USER_ID = ${user_id}`;
    const result = await database.query(query);
    const {USER_FIRSTNAME, USER_LASTNAME, USER_BIRTHDATE, JOB_NAME, JOB_STATUS} = result[0];
    const profil = new Profil(USER_FIRSTNAME, USER_LASTNAME, USER_BIRTHDATE, JOB_NAME, JOB_STATUS);
    throw profil;
};

const QUERY_profil_update = async (profil_data)=> {
 

    const user_id = 1;

    const tab = [];

    const test = _.mapObject(profil_data.user_info, (val, key) => {
        tab.push(`SET ${key.toUpperCase()} = '${val}'`);
    });

    console.log(tab);
    const join = tab.join(', ');
   
    const to_update = Object.keys(profil_data)[0];
    let who2 = '';
    if (to_update === 'user_info') {
        who2 = 'USER';
    } else if (to_update=== 'job_info') {
        who2 = 'JOB';
    }else if (to_update === 'auth_info') {
        who2 = 'AUTH';
    }
      
    const query = `UPDATE
    ${who2}
    ${join}
    WHERE USER_ID = ${user_id}`;

    console.log(query);
    return profil_data;
};





export {
    QUERY_profil_by_id,
    QUERY_profil_update,
};