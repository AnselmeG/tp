import {
    database
} from '../../../DATABASE.js';

const QUERY_is_exp_exist = async (register_data) => {
    const is_exp_exist = `SELECT 
    EXP_NAME, EXP_CIE, 
    EXP_BEGIN, EXP_END, 
    EXP_CONTRACT, EXP_DESCRIPTION
    FROM EXPERIENCE
    WHERE EXP_NAME = '${register_data.exp_name}'`;
    return database.query(is_exp_exist);
};

const QUERY_create_dep = async (register_data) => {
    const create_dep = `INSERT
    INTO DEP  (
        DEP_CODE, 
    )
    VALUES (
        '${register_data.exp_dep}'   
    )`;
    return database.query(create_dep);
};

const QUERY_dep_by_id = async (register_data) =>{
    const dep_by_id = `SELECT DEP_ID FROM DEP
    WHERE DEP_CODE = ${register_data.exp_dep}`;

    return database.query(dep_by_id);
};

const QUERY_profil_id = async(user_id) =>{
    const profil_id = `SELECT PROFIL_ID FROM USER 
    WHERE PROFIL_ID = ${user_id}`;

    return database.query(profil_id);
};

const QUERY_create_exp = async (register_data, user_id) => {

    const raw_dep_id = await QUERY_dep_by_id(register_data);
    const {DEP_ID} = raw_dep_id[0];

    const raw_profil_id = await QUERY_profil_id(user_id);
    const {PROFIL_ID} = raw_profil_id[0];

    const create_exp = `INSERT 
    INTO EXPERIENCE (
        EXP_NAME, 
        EXP_CIE, 
        EXP_BEGIN, 
        EXP_END, 
        EXP_CONTRACT, 
        EXP_DESCRIPTION,
        PROFIL_ID,
        DEP_ID
    )
    VALUES (
        '${register_data.exp_name}', 
        '${register_data.exp_cie}', 
        '${register_data.exp_begin}', 
        '${register_data.exp_end}',
        '${register_data.exp_contract}',
        '${register_data.exp_desc}',
        ${PROFIL_ID},
        ${DEP_ID}
    )`;
    return database.query(create_exp);
};

const QUERY_exp_register = async (register_data, user_id) => {
    const exist = await QUERY_is_exp_exist(register_data);
    if (exist.length) {
        throw {
            _state: 'conflict',
            _conflict: 'exp already exist!',
        };
    } else {
        await QUERY_create_exp(register_data, user_id);
        throw {
            _state: 'created',
            _created: 'New exp created',
        };
    }
};
export {
    QUERY_is_exp_exist,
    QUERY_create_dep,
    QUERY_dep_by_id,
    QUERY_profil_id,
    QUERY_create_exp,
    QUERY_exp_register,
};