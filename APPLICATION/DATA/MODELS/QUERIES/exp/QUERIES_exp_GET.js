import {
    database
} from '../../../DATABASE.js';


const QUERY_exp_by_id = async (user_id) => {
    try {
        const query = `SELECT 
        EXP_ID, EXP_NAME, EXP_CIE, EXP_BEGIN, EXP_END, EXP_CONTRACT ,EXP_DESCRIPTION, 
        PROFIL_ID, DEP.DEP_NAME
        FROM EXPERIENCE
        JOIN DEP ON EXPERIENCE.DEP_ID = DEP.DEP_ID
        WHERE PROFIL_ID = ${user_id}`;
        const result = await database.query(query);
        throw result;
    } catch (e) {
        throw e;
    } 
};

export {
    QUERY_exp_by_id,
};