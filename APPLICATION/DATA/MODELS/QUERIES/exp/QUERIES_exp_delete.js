import {
    database
} from '../../../DATABASE.js';


const QUERY_exp_if_exist =  async (exp_id) =>{
    const query_if_exist =`SELECT 
    EXP_NAME, EXP_CIE, 
    EXP_BEGIN, EXP_END, 
    EXP_CONTRACT, EXP_DESCRIPTION
    FROM EXPERIENCE
    WHERE EXP_ID = '${exp_id}'`;
    const result = await database.query(query_if_exist);
};

const QUERY_exp_delete =  async (exp_id) =>{
    const query = `DELETE FROM EXPERIENCE 
    WHERE EXP_ID = ${exp_id} `;
    const result = await database.query(query);
    throw result;
};


const QUERY_exp_delete_if_exist = async () => {
    const exist = await QUERY_exp_if_exist();
    if (!exist.length) {
        throw {
            _state: 'conflict',
            _conflict: 'exp not exist!',
        };
    } else {
        await QUERY_exp_delete();
        throw {
            _state: 'deleted',
            _deleted: 'Experience deleted!'
            
        };
    }
};
export {
    QUERY_exp_delete,
    QUERY_exp_if_exist,
    QUERY_exp_delete_if_exist
};