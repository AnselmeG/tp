import {
    database
} from '../../../DATABASE.js';

const QUERY_users_list = async () => {
    const query = 'SELECT * FROM USER';
    throw await database.query(query);
};

const QUERY_users_by_id = async (user_id) => {
    const query = `SELECT * FROM USER WHERE USER_ID = ${user_id}`;
    throw await database.query(query);
};

export {
    QUERY_users_by_id,
    QUERY_users_list,
};