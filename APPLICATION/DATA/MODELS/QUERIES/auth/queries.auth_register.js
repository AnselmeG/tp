import {
    database
} from '../../../DATABASE.js';

const QUERY_is_user_exist = (register_data) => {
    const is_user_exist = `SELECT 
    USER_FIRSTNAME, USER_LASTNAME,
    USER_BIRTHDATE, USER_MAIL
    FROM USER
    WHERE USER_MAIL = '${register_data.user_mail}'`;
    return database.query(is_user_exist);
};

const QUERY_create_password = (hash) => {
    const create_password = `INSERT
    INTO AUTH (
        AUTH_PASSWORD
    )
    VALUES (
        '${hash}'
    )`;
    return database.query(create_password);
};

const QUERY_create_job = (register_data) => {
    const create_job = `INSERT
    INTO JOB (
        JOB_NAME,
        JOB_STATUS
    )
    VALUES (
        '${register_data.job_name}',
        '${register_data.job_status}'
    )`;
    return database.query(create_job);
};

const QUERY_create_profil = (insert_job) => {
    const create_profil = `INSERT
    INTO PROFIL (
        JOB_ID
    )
    VALUES (
        ${insert_job.insertId}
    )`;
    return database.query(create_profil);
};

const QUERY_create_user = (register_data, insert_passwd, insert_profil) => {
    const create_user = `INSERT 
    INTO USER (
        USER_FIRSTNAME, 
        USER_LASTNAME, 
        USER_BIRTHDATE, 
        USER_MAIL, 
        AUTH_ID, 
        PROFIL_ID
    )
    VALUES (
        '${register_data.user_firstname}', 
        '${register_data.user_lastname}', 
        '${register_data.user_birthdate}', 
        '${register_data.user_mail}',
        ${insert_passwd.insertId},
        ${insert_profil.insertId}
    )`;
    return database.query(create_user);
};



export {
    QUERY_create_job,
    QUERY_create_password,
    QUERY_create_user,
    QUERY_is_user_exist,
    QUERY_create_profil,
};