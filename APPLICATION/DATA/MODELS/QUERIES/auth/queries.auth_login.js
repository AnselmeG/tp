import {
    database
} from '../../../DATABASE.js';

const QUERY_find_user_by_mail = async (login_data) => {
    const query = `SELECT
    USER_ID FROM USER
    WHERE USER_MAIL = '${login_data.user_login}'`;
    const user_id = await database.query(query);
    return user_id[0].USER_ID;
};

const QUERY_find_hash_by_id = async (user_id) => {
    const query = `SELECT
    AUTH.AUTH_PASSWORD FROM USER
    JOIN AUTH ON USER.AUTH_ID = AUTH.AUTH_ID
    WHERE USER_ID = ${user_id}`;
    const hash = await database.query(query);
    return hash[0].AUTH_PASSWORD;
};

export {
    QUERY_find_hash_by_id,
    QUERY_find_user_by_mail,
};