import bcrypt from 'bcrypt';

import * as register from '../auth/queries.auth_register.js';
import * as login from '../auth/queries.auth_login.js';

const QUERY_auth_register = async (register_data) => {
    const exist = await register.QUERY_is_user_exist(register_data);
    if (exist.length) {
        throw {
            _state: 'conflict',
            _conflict: 'User already exist!',
        };
    } else {
        const hash = await bcrypt.hash(register_data.user_password, 10);
        const insert_passwd = await register.QUERY_create_password(hash);
        const insert_job = await register.QUERY_create_job(register_data);
        const insert_profil = await register.QUERY_create_profil(insert_job);
        await register.QUERY_create_user(register_data, insert_passwd, insert_profil);
        throw {
            _state: 'created',
            _created: 'New user created',
        };
    }
};

const QUERY_auth_login = async (login_data) => {
    const user_id = await login.QUERY_find_user_by_mail(login_data);
    const hash = await login.QUERY_find_hash_by_id(user_id);
    const match = await bcrypt.compare(login_data.user_password, hash);
    if (!match) {
        throw 'forbidden';
    } else {
        throw {
            _state: 'created',
            _user_id: user_id,
        };
    }
};

export {
    QUERY_auth_login,
    QUERY_auth_register,
};