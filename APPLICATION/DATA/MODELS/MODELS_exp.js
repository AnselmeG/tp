import {
    QUERY_exp_register
} from '../MODELS/QUERIES/exp/QUERIES_exp_register.js';

import {
    QUERY_exp_by_id
} from '../MODELS/QUERIES/exp/QUERIES_exp_GET.js';

import {
    QUERY_exp_delete
} from '../MODELS/QUERIES/exp/QUERIES_exp_delete.js';


const MODEL_exp = async (user_id) => {
    try {
        await QUERY_exp_by_id(user_id);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state: 'internal_server',
                _internal_server : resource.sqlMessage
            };
        } else {
            throw {
                _state: 'found',
                _resource: resource
            };
        }
    } 
};

const MODEL_exp_register = async (register_data, user_id) => {
    try {
        await QUERY_exp_register(register_data, user_id);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state : 'internal_server',
                _internal_server : resource.sqlMessage,
            };
        } else if (resource._state === 'created') {
            throw {
                _state : 'created',
                _created : 'Experience created',
            };
        } else {
            throw {
                _state : 'conflict',
                _conflict : 'Experience already exit'
            };
        }
    }
};

const MODEL_exp_delete = async (exp_id) => {
    try {
        await QUERY_exp_delete(exp_id);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state: 'internal_server',
                _internal_server : resource.sqlMessage
            };
        } else if (resource.affectedRows === 0) { 
            throw {
                _state: 'not_found',
                _not_found: 'Experience not found'
            }
        } else {
            throw {
                _state: 'deleted'
            };
        }
    } 
};

export {
    MODEL_exp_register,
    MODEL_exp,
    MODEL_exp_delete,
};