import jwt from 'jsonwebtoken';

import {
    QUERY_auth_login,
    QUERY_auth_register,
} from '../MODELS/QUERIES/auth/QUERIES_auth.js';

const generate_token = (uid) => {
    const token  = jwt.sign({ data: uid }, 'Secret');
    return token;
};

const MODEL_auth_login = async (login_data) => {
    try {
        await QUERY_auth_login(login_data);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state : 'internal_server',
                _internal_server : resource.sqlMessage,
            };
        } else if (resource._state === 'created') {
            const token = generate_token(resource._user_id);
            throw {
                _state : 'created',
                _created : {
                    token : `${token}`,
                }
            };
        } else {
            throw {
                _state : 'forbidden',
                _forbidden : 'Incorrect password or login, please verify it!'
            };
        }
    }
};

const MODEL_auth_register = async (register_data) => {
    try {
        await QUERY_auth_register(register_data);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state : 'internal_server',
                _internal_server : resource.sqlMessage,
            };
        } else if (resource._state === 'created') {
            throw {
                _state : 'created',
                _created : 'User created',
            };
        } else {
            throw {
                _state : 'conflict',
                _conflict : 'User already exit'
            };
        }
    }
};

export {
    MODEL_auth_login,
    MODEL_auth_register,
};