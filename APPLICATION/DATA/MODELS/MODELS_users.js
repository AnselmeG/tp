import {
    QUERY_users_by_id,
    QUERY_users_list,
} from '../MODELS/QUERIES/users/QUERIES_users.js';

const MODEL_users_list = async () => {
    try {
        await QUERY_users_list();
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state: 'internal_server',
                _internal_server : resource.sqlMessage
            };
        } else {
            throw {
                _state: 'found',
                _resource: resource
            };
        }
    } 
};

const MODEL_users_by_id = async (user_id) => {
    try {
        await QUERY_users_by_id(user_id);
    } catch (resource) {
        if (resource.sqlMessage) {
            throw {
                _state: 'internal_server',
                _internal_server: resource.sqlMessage,
            };
        } else if (resource.length){
            throw {
                _state: 'found',
                _resource: resource[0],
            };
        } else {
            throw {
                _state: 'not_found',
                _not_found : 'User not found!',
            };
        }
    }
};

export {
    MODEL_users_by_id,
    MODEL_users_list,
};