import jwt from 'jsonwebtoken';

const SERVICE_authguard = (req, res, next) => {
    // let _SECRET_KEY = 'Secret';
    let { token } = req.params;
    try {
        if (token) {
            req._user_id = jwt.verify(token, 'Secret').data;
        }
        req._state = 'accepted';
    } catch (e) {
        req._state = 'bad_params';
        req._bad_params = e.message;
    }
    next();
};

export {
    SERVICE_authguard,
};