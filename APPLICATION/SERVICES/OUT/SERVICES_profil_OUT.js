import {
    MODEL_profil,
    MODEL_profil_update
} from '../../DATA/MODELS/MODELS_profil.js';

const SERVICE_profil_OUT = async (req, res, next) => {
    try {
        const user_id = req._user_id;
        await MODEL_profil(user_id);
    } catch (resource) {
        req._state = resource._state;
        req._not_found = resource._not_found;
        req._internal_server = resource._internal_server;
        req._resource = resource._resource;
    } finally {
        next();
    }
};

const SERVICE_profil_update_OUT = async (req, res, next) => {
    try {
        const profil_data = req._profil_data;
        await MODEL_profil_update(profil_data);
    } catch (resource) {
        req._state = resource._state;
        req._not_found = resource._not_found;
        req._internal_server = resource._internal_server;
        req._resource = resource._resource;
    } finally {
        next();
    }
};

export {
    SERVICE_profil_OUT,
    SERVICE_profil_update_OUT,
};