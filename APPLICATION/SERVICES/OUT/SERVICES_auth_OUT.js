import {
    MODEL_auth_register,
    MODEL_auth_login,
} from '../../DATA/MODELS/MODELS_auth.js';

const SERVICE_auth_register_OUT = async (req, res, next) => {
    try {
        const register_data = req.body;
        await MODEL_auth_register(register_data);
    } catch (resource) {
        req._state = resource._state;
        req._conflict = resource._conflict;
        req._created = resource._created;
        req._internal_server = resource._internal_server;
    } finally {
        next();
    }
};

const SERVICE_auth_login_OUT = async (req, res, next) => {
    try {
        const login_data = req.body;
        await MODEL_auth_login(login_data);
    } catch (resource) {
        req._state = resource._state;
        req._created = resource._created;
        req._forbidden = resource._forbidden;
        req._internal_server = resource._internal_server;
    } finally {
        next();
    }
};

export {
    SERVICE_auth_login_OUT,
    SERVICE_auth_register_OUT
};