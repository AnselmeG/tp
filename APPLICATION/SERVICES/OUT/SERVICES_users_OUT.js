import {
    MODEL_users_by_id,
    MODEL_users_list,
} from '../../DATA/MODELS/MODELS_users.js';

const SERVICE_users_list_OUT = async (req, res, next) => {
    try {
        await MODEL_users_list();
    } catch (resource) {
        req._state = resource._state;
        req._internal_server = resource._internal_server,
        req._resource = resource._resource;
    } finally {
        next();
    }
};

const SERVICE_users_by_id_OUT = async (req, res, next) => {
    try {
        const user_id = req._user_id;
        await MODEL_users_by_id(user_id);
    } catch (resource) {
        req._state = resource._state;
        req._not_found = resource._not_found;
        req._internal_server = resource._internal_server;
        req._resource = resource._resource;
    } finally {
        next();
    }
};

export {
    SERVICE_users_by_id_OUT,
    SERVICE_users_list_OUT,
};