import {
    MODEL_exp_register,
    MODEL_exp,
    MODEL_exp_delete
} from '../../DATA/MODELS/MODELS_exp.js';

const SERVICE_exp_OUT = async (req, res, next) => {
    try {
        const user_id = req._user_id;
        await MODEL_exp(user_id);
    } catch (resource) {
        req._state = resource._state;
        req._not_found = resource._not_found;
        req._internal_server = resource._internal_server;
        req._resource = resource._resource;
    } finally {
        next();
    }
};
   

const SERVICE_exp_register_OUT = async (req, res, next) => {
    try {
        const register_data = req.body;
        console.log(req._user_id);
        const user_id = req._user_id;
        await MODEL_exp_register(register_data, user_id);
    } catch (resource) {
        req._state = resource._state;
        req._conflict = resource._conflict;
        req._created = resource._created;
        req._internal_server = resource._internal_server;
    } finally {
        next();
    }
};

const SERVICE_exp_delete_OUT = async (req, res, next) => {
    try {
        const exp_id = req.body.exp_id;
        await MODEL_exp_delete(exp_id);
    } catch (resource) {
        req._state = resource._state;
        req._conflict = resource._conflict;
        req._deleted = resource._deleted;
        req._not_found = resource._not_found;
        req._internal_server = resource._internal_server;
    } finally {
        next();
    }
};


export {
    SERVICE_exp_register_OUT,
    SERVICE_exp_OUT,
    SERVICE_exp_delete_OUT
};