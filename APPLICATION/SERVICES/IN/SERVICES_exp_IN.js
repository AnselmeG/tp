import Joi from 'joi';
import {
    SCHEMA_exp_register
} from './SCHEMAS/SCHEMAS_exp.js';


const SERVICE_exp_IN = (req, res, next) => {
    try {
        req._state = 'accepted';
    } catch (e) {
        throw e;
    }
    next();
};




const SERVICE_exp_register_IN = (req, res, next) => {
    try {
        const register_data = req.body;
        console.log(req._user_id);
        const exp_schema = SCHEMA_exp_register();
        Joi.validate(register_data, exp_schema, (err) => {
            if (err) {
                req._state = 'bad_params';
                req._bad_params = 'Schema send is not correct, please verify it!';
            } else {
                req._state = 'accepted';
            }
        });
    } catch (e) {
        throw e;
    }
    next();
};
export {
    SERVICE_exp_register_IN,
    SERVICE_exp_IN,
     
};