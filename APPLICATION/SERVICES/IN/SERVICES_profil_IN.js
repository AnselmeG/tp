import Joi from 'joi';
import {
    SCHEMA_profil_update,
} from './SCHEMAS/SCHEMAS_profil.js';

const SERVICE_profil_IN = (req, res, next) => {
    try {
        req._state = 'accepted';
    } catch (e) {
        throw e;
    }
    next();
};

const SERVICE_profil_update_IN = (req, res, next) => {
    try {
        const profil_data = req.body;
        const to_update =  Object.keys(profil_data)[0];
        const data = profil_data[to_update];

        const profil_schema = SCHEMA_profil_update(profil_data);
        Joi.validate(data, profil_schema, (err) => {
            if (err) {
                req._state = 'bad_params';
                req._bad_params = 'Schema send is not correct, please verify it!';
            } else {
                req._state = 'accepted';
                req._profil_data = profil_data;
            }
        });
    } catch (e) {
        throw e;
    }
    next();
};

export {
    SERVICE_profil_IN,
    SERVICE_profil_update_IN,
    
};