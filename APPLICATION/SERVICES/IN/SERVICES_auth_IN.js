import Joi from 'joi';
import {
    SCHEMA_auth_register,
    SCHEMA_auth_login
} from './SCHEMAS/SCHEMAS_auth.js';

const SERVICE_auth_login_IN = (req, res, next) => {
    try {
        const login_data = req.body;
        const auth_schema = SCHEMA_auth_login();
        Joi.validate(login_data, auth_schema, (err) => {
            if (err) {
                req._state = 'bad_params';
                req._bad_params = 'Schema send is not correct, please verify it!';
            } else {
                req._state = 'accepted';
            }
        });
    } catch (e) {
        throw e;
    }
    next();
};

const SERVICE_auth_register_IN = (req, res, next) => {
    try {
        const register_data = req.body;
        const auth_schema = SCHEMA_auth_register();
        Joi.validate(register_data, auth_schema, (err) => {
            if (err) {
                req._state = 'bad_params';
                req._bad_params = 'Schema send is not correct, please verify it!';
            } else {
                req._state = 'accepted';
            }
        });
    } catch (e) {
        throw e;
    }
    next();
};

export {
    SERVICE_auth_register_IN,
    SERVICE_auth_login_IN,
};