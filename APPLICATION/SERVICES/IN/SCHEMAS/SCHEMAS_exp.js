import Joi from 'joi';

const SCHEMA_exp_register  = () => {
    return Joi.object().keys({
        exp_name: Joi.string().required(),
        exp_begin: Joi.string().required(),
        exp_end: Joi.string().required(),
        exp_cie: Joi.string().required(),
        exp_desc: Joi.string().required(),
        exp_contract: Joi.string().required(),
        exp_dep: Joi.string().required(),
    });
};


export {
    SCHEMA_exp_register,
};