import Joi from 'joi';


const SCHEMA_profil_update  = (profil_data) => {

    const test =  Object.keys(profil_data)[0];
    console.log(test);
    if (test === 'user_info') {
        return Joi.object().keys({
            user_firstname: Joi.string().required(),
            user_lastname: Joi.string(),
            user_birthdate: Joi.string(),
            user_mail: Joi.string(),
        });
    } else if (test === 'job_info') {
        return Joi.object().keys({
            job_name: Joi.string(),
            job_status: Joi.string(),
        });
    } else if (test === 'auth_info') {
        return Joi.object().keys({
            auth_password: Joi.string(),
        });
    }

};

export {
    SCHEMA_profil_update,
};