import Joi from 'joi';

const SCHEMA_auth_login  = () => {
    return Joi.object().keys({
        user_login: Joi.string().required(),
        user_password: Joi.string().required(),
    });
};

const SCHEMA_auth_register  = () => {
    return Joi.object().keys({
        user_firstname: Joi.string().required(),
        user_lastname: Joi.string().required(),
        user_mail: Joi.string().required(),
        user_birthdate: Joi.string().required(),
        user_password: Joi.string().required(),
        job_name: Joi.string().valid('FRONTEND', 'FULLSTACK', 'BACKEND').required(),
        job_status: Joi.string().valid('EN POSTE', 'EN RECHERCHE', 'EN VEILLE').required(),
    });
};

export {
    SCHEMA_auth_login,
    SCHEMA_auth_register,
};