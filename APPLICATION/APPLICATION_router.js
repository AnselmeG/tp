import {
    ROUTES_profil
} from './CONTROLLER/ROUTES/ROUTES_profil.js';

import {
    ROUTES_users
} from './CONTROLLER/ROUTES/ROUTES_users.js';
import {
    ROUTES_auth
} from './CONTROLLER/ROUTES/ROUTES_auth.js';
import {
    ROUTES_exp
} from './CONTROLLER/ROUTES/ROUTES_exp.js';

const APPLICATION_router = (app) => {
    ROUTES_users(app);
    ROUTES_auth(app);
    ROUTES_profil(app);
    ROUTES_exp(app);
};

export {
    APPLICATION_router,
};