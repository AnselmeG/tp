/*
 ** For fun ...
 */

// les imports par défaut
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';


// les imports personnalisés
import {
    APPLICATION_server
} from './APPLICATION_server.js';
import {
    APPLICATION_router
} from './APPLICATION_router.js';

// déclaration des variables
const app = express();
const log = morgan('dev');
const srv = new APPLICATION_server(app);

// initialisation du server
srv.setEnv(app, process.env.PORT, '0.0.0.0');
srv.run(app);

/*
 ** début de l'application.
 */

// utilisation du moddileware bodyparser,
app.use(bodyParser.urlencoded({
    extended: true,
    mergeParams: true
}));
app.use(bodyParser.json());


// utilisation du middleware log
app.use(log);

// utilisation du middleware pour activers les cors origins // Pour angular
app.use(cors());

// initialisation des routes
APPLICATION_router(app);

// todo convertir en ROUTE_not_found(app)
app.use((req, res) => {
    res.status(404).json('url not found');
    res.end();
});

export {
    app,
};