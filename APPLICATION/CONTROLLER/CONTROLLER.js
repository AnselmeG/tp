// controller de l'application
const RESPONSE_controller = (req, res, next) => {
    let request = {
        'accepted': () => next(),
        'found': () => res.status(200).json(req._resource),
        'deleted': () => res.status(204).json(req._deleted),
        'not_found': () => res.status(404).json(req._not_found),
        'bad_params': () => res.status(400).json(req._bad_params),
        'conflict': () => res.status(409).json(req._conflict),
        'created': () => res.status(201).json(req._created),
        'forbidden': () => res.status(403).json(req._forbidden),
        'internal_server': () => res.status(500).json(req._internal_server),
    };
    let response_controller = (req) => request[req._state]();
    response_controller(req);
};

export {
    RESPONSE_controller,
};