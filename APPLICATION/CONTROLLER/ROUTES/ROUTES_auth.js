import { SERVICE_auth_register_IN, SERVICE_auth_login_IN } from '../../SERVICES/IN/SERVICES_auth_IN.js';
import { SERVICE_auth_register_OUT, SERVICE_auth_login_OUT} from '../../SERVICES/OUT/SERVICES_auth_OUT.js';
import { RESPONSE_controller } from '../../CONTROLLER/CONTROLLER.js';

const ROUTES_auth = (app) => {

    // enregistrer un nouvel utilisateur
    app.route('/auth/register')

        .post(
            SERVICE_auth_register_IN,
            RESPONSE_controller,
            SERVICE_auth_register_OUT,
            RESPONSE_controller,
        );
    
    // connecter un nouvel utilisateur
    app.route('/auth/login')

        .post(
            SERVICE_auth_login_IN,
            RESPONSE_controller,
            SERVICE_auth_login_OUT,
            RESPONSE_controller,
        );

};

export {
    ROUTES_auth,
};