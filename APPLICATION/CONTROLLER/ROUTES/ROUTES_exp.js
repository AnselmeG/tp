import { SERVICE_exp_register_OUT, SERVICE_exp_OUT, SERVICE_exp_delete_OUT } from '../../SERVICES/OUT/SERVICES_exp_OUT.js';
import { SERVICE_exp_register_IN } from '../../SERVICES/IN/SERVICES_exp_IN.js';
import { RESPONSE_controller } from '../../CONTROLLER/CONTROLLER.js';
import { SERVICE_authguard} from '../../SERVICES/SERVICES.js';

const ROUTES_exp = (app) => {

    // enregistrer une nouvelle experience
    app.route('/exp/:token/register')

        .post(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_exp_register_IN,
            RESPONSE_controller,
            SERVICE_exp_register_OUT,
            RESPONSE_controller,
        );  
        
    app.route('/exp/:token/show_one')

        .get(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_exp_OUT,
            RESPONSE_controller,
        );
        
    app.route('/exp/:token/delete')

        .delete(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_exp_delete_OUT,
            RESPONSE_controller,
        );        
};
export {
    ROUTES_exp,
};