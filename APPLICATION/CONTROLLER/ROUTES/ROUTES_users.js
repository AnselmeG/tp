import { SERVICE_authguard } from '../../SERVICES/SERVICES.js';
import { SERVICE_users_by_id_OUT, SERVICE_users_list_OUT} from '../../SERVICES/OUT/SERVICES_users_OUT.js';
import { RESPONSE_controller } from '../../CONTROLLER/CONTROLLER.js';

const ROUTES_users = (app) => {

    // Voir la liste des utilisateurs
    app.route('/users/:token/show_all')

        .get(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_users_list_OUT,
            RESPONSE_controller,
        );

    // Voir un utilisateur
    app.route('/users/:token/show_one')
        .get(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_users_by_id_OUT,
            RESPONSE_controller,
        );
};

export {
    ROUTES_users
};