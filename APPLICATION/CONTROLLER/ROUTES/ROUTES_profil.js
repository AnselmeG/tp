import { SERVICE_authguard} from '../../SERVICES/SERVICES.js';
import { SERVICE_profil_OUT, SERVICE_profil_update_OUT } from '../../SERVICES/OUT/SERVICES_profil_OUT.js';
import { SERVICE_profil_update_IN } from '../../SERVICES/IN/SERVICES_profil_IN.js';
import { RESPONSE_controller } from '../../CONTROLLER/CONTROLLER.js';

const ROUTES_profil = (app) => {

    // enregistrer un nouvel utilisateur
    app.route('/profil/:token/show_one')

        .get(
            SERVICE_authguard,
            RESPONSE_controller,
            SERVICE_profil_OUT,
            RESPONSE_controller,
        );
    
    // connecter un nouvel utilisateur
    app.route('/profil/:token/update_one')

        .put(
            SERVICE_profil_update_IN,
            RESPONSE_controller,
            SERVICE_profil_update_OUT,
            RESPONSE_controller,
        );
};

export {
    ROUTES_profil,
};