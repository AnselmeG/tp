const port = '3000';
const host = '0.0.0.0';

export class APPLICATION_server {

    constructor(app) {
        this.port = port;
        this.host = host;
        this.app = app;
    }

    run(app) {
        this.app.listen(this.port, this.host, () => {
            console.log(`Server started on ${this.host}${this.port}`);
        });
    }

    setEnv(app, new_port, new_host) {
        this.port = new_port;
        this.host = new_host;
    }

}